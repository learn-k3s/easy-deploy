#!/bin/sh

if [[ "$1" == "version" ]]; then
  echo "1.0.0"
  exit 0
fi

if [[ "$1" == "config" ]]; then
  echo "$KUBECONFIG"
  exit 0
fi

if [[ "$1" == "deploy" ]]; then

  export CONTAINER_PORT=${CONTAINER_PORT:-8080}
  export EXPOSED_PORT=${EXPOSED_PORT:-80}
  export APPLICATION_NAME=$(basename $(git rev-parse --show-toplevel))
  export DOCKER_USER="${DOCKER_USER}"
  export NAMESPACE="${NAMESPACE}"
  export IMAGE_NAME="${APPLICATION_NAME}-img"
  export TAG=$(git rev-parse --short HEAD)
  export IMAGE="${DOCKER_USER}/${IMAGE_NAME}:${TAG}"
  export CLUSTER_IP="${CLUSTER_IP}"
  export BRANCH=$(git symbolic-ref --short HEAD)
  export HOST="${APPLICATION_NAME}.${BRANCH}.${CLUSTER_IP}.nip.io"

  docker build -t ${IMAGE_NAME} .
  docker tag ${IMAGE_NAME} ${IMAGE}
  docker push ${IMAGE}

  envsubst < ./kube/deploy.template.yaml > ./kube/deploy.${TAG}.yaml

  kubectl apply -f ./kube/deploy.${TAG}.yaml -n ${NAMESPACE}
  echo "🌍 http://${HOST}"

  exit 0
fi


