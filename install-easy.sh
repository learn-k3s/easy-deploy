#!/bin/sh
# https://kubernetes.io/fr/docs/reference/kubectl/overview/#exemples-cr%c3%a9er-et-utiliser-des-plugins
sudo chmod +x ./kubectl-easy.sh

# et déplacez-le dans un répertoire de votre PATH
cp ./kubectl-easy.sh ./kubectl-easy
sudo mv ./kubectl-easy /usr/local/bin

