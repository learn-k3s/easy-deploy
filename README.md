# easy-deploy

```shell
export KUBECONFIG=../create-cluster/k3s.yaml
REPLICAS=3 \
NAMESPACE="training" \
DOCKER_USER="k33g" \
CLUSTER_IP="192.168.64.17" \
kubectl easy deploy
```
